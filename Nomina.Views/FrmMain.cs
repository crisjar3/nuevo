﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> empleados = imp.All();
            MessageBox.Show("profesor me equivoque con lo de los 5 mas y altos pense que era ordenacion del mas bajo a alto y viceversa");
            loadpe(empleados);
        }

        public void loadpe(List<Empleado> empleadosL)
        {
            int i = 0;
            foreach (Empleado emp in empleadosL)
            {
                dgvEmpleados.Rows.Add();
                dgvEmpleados.Rows[i].Cells[0].Value = emp.Id;

                dgvEmpleados.Rows[i].Cells[1].Value = emp.Cedula;
                dgvEmpleados.Rows[i].Cells[2].Value = emp.Nombres;
                dgvEmpleados.Rows[i].Cells[3].Value = emp.Apellidos;
                dgvEmpleados.Rows[i].Cells[4].Value = emp.Direccion;
                dgvEmpleados.Rows[i].Cells[5].Value = emp.Telefono;
                dgvEmpleados.Rows[i].Cells[6].Value = emp.FechaContratacion;
                dgvEmpleados.Rows[i].Cells[7].Value = emp.Salario;
                dgvEmpleados.Rows[i].Cells[8].Value = emp.Foto;

                i++;


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> op = imp.All();
            List<Empleado> SortedList = op.OrderBy(o => o.Salario).ToList();
            List<Empleado> SortedListi = new List<Empleado>();
            int k = 99;
            foreach (Empleado lop in SortedList)
            {
                SortedListi.Add(SortedList[k]);
                
                k--;
                
            }
            loadpe(SortedListi);


        }

        private void button5_Click(object sender, EventArgs e)
        {

            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> empleados = imp.All();
            loadpe(empleados);
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show( promedio().ToString());

        }

        public Decimal promedio()
        {
            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> op = imp.All();
            decimal n = 0;
            foreach (Empleado l in op)
            {
                n = n + l.Salario;
            }
            n = n / 100;
            
            return n;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> op = imp.All();
            List<Empleado> x = new List<Empleado>();
            decimal n = promedio();
            foreach (Empleado l in op)
            {
                if (n<l.Salario)
                {
                    x.Add(l);
                    
                }
            }

            loadpe(x);
           

                                                                    
           
           


        }

        private void button2_Click(object sender, EventArgs e)
        {
            DaoEmpleadoImplements imp = new DaoEmpleadoImplements();
            List<Empleado> op = imp.All();
            List<Empleado> SortedList = op.OrderBy(o => o.Salario).ToList();
            
            
            loadpe(SortedList);


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmEmpleado k = new FrmEmpleado();
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder mostrar su salario Neto", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (rowCollection.Count > 0)
            {

                int index = dgvEmpleados.SelectedRows[0].Index;
                Decimal salario = Decimal.Parse(dgvEmpleados.Rows[index].Cells[7].Value.ToString());
                k.set(salario);
                k.Show();

            }
        }
    }
}
