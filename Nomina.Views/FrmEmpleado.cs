﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        

        public FrmEmpleado()
        {
            
            InitializeComponent();
        }
        public void set(Decimal l)
        {
            double u =  (double.Parse(l.ToString()) * 12*0.0625);
            double anual = double.Parse(l.ToString()) * 12;
            double ir= new double();
            ir = 0;

            txtBruto.Text = l.ToString();
            txtInss.Text = (u/12).ToString();
            if (anual - u > 20000 && anual -u <300000)
            {
                ir = u * 12 *0.15;

                txtIr.Text = (ir/12).ToString();
            }else if(anual - u> 30000)
             {
                ir = u * 12 * 0.2;

                txtIr.Text = (ir/12).ToString();

            }
            else
            {
                txtIr.Text = "0";
            }

            double netos = double.Parse(l.ToString()) - (u/12) - (ir/12);
            txtNeto.Text = netos.ToString();

        }


        private void button2_Click(object sender, EventArgs e)
        {
        }
    }
}
