﻿using Nomina.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Business
{
    class Program
    {
        static void Main(string[] args)
        {
            Implements.DaoEmpleadoImplements daoEmpleado = new Implements.DaoEmpleadoImplements();

            Empleado pepito = new Empleado()
            {
                Nombres = "Pepito",
                Apellidos = "Perez",
                Cedula = "001-090900-0000U",
                Direccion = "Por la estacion de bomberos",
                Telefono = "2278-9900",
                FechaContratacion ="" /*new DateTime(2018, 08, 01)*/,
                Salario = 23000
            };

            //daoEmpleado.Create(pepito);
            List<Empleado> empleados = daoEmpleado.All();

            empleados.ForEach(e =>
            {
                Console.WriteLine(e.ToString());
            });

            Console.Read();
        }
    }
}
