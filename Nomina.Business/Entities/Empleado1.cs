﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Business.Entities
{
    public class Empleado1
    {
        public int Id { get; set; } //4
        public string Cedula { get; set; } //32 + 3
        public string Nombres { get; set; } // 40 + 3
        public string Apellidos { get; set; } //40 + 3
        public string Direccion { get; set; } //400 + 3
        public string Telefono { get; set; } //30 + 3
        public String FechaContratacion { get; set; } // 8
        public decimal Salario { get; set; } //8
        public string Foto { get; set; } // 400 + 3

        public override string ToString()
        {
            return $"Id: {Id}, Nombres: {Nombres}, Apellidos: {Apellidos}, Fecha Contratacion: {FechaContratacion}, Salario: {Salario}";
        }

        public object[] EmpleadoAsArray()
        {
            return new object[] { Id, Cedula, Nombres, Apellidos, Direccion, Telefono, FechaContratacion, Salario };
        }
    }
}
